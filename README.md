<h1>PCCT Mouse Brain Analysis</h1>

<p>This is the repository for data and results from the manuscript:</p>

<p><strong>Nadkarni, R.; Han, Z.Y.; Anderson, R.J.; Allphin, A.J.; Clark, D.P.; Badea, A.; Badea, C.T. High-Resolution Hybrid Micro-CT Imaging Pipeline for Mouse Brain Region Segmentation and Volumetric Morphometry. Plos One 2024 </strong></p>

<p>This repository includes the NIfTI files for our new PCD CT atlas brain (named QIAL_CT_MB) and its labels. In addition, we have included the file <strong>labels_lookup.txt</strong> that indicates the brain region corresponding to each label number in the label map. Please note that label numbers 1-166 refer to brain regions in the left hemisphere while label numbers 1001-1166 refer to the same brain regions in the right hemisphere.</p>

<p>This repository also includes the results of our region-based analysis comparing all 330 brain regions and total brain volume between male and female mice across all genotypes using 2-sample t-tests with false discovery rate (FDR) correction of p-values. The 26 significant regions from these t-tests are summarized in Table 4 of the manuscript. </p>

<p>The NIfTI files for the diffusion-weighted MRI (DWI) atlas shown in <strong>Figure 10c</strong> and corresponding labels shown in <strong>Figure 10d</strong> of the manuscript are available at the following link: </p>

<p> https://zenodo.org/records/10652239 </p>

<p>To access source code for the Small Animal Multivariate Brain Analysis (SAMBA) tool that we used for brain region segmentation, please see the Information Sharing Statement section of the original manuscript discussing this tool:</p>

<p><strong>Anderson, R.J.; Cook, J.J.; Delpratt, N.; Nouls, J.C.; Gu, B.; McNamara, J.O. et al. Small Animal Multivariate Brain Analysis (SAMBA) -- a High Throughput Pipeline with a Validation Framework. Neuroinformatics 2018. </strong> </p>

<p>An image showing coronal, sagittal, and axial views of the PCD CT atlas both with and without brain region labels overlayed is copied below:</p>
![PCDatlas](PCDatlas.png)
